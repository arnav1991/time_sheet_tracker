var express = require('express');
var router = express.Router();

userController = require('./controller.js');

//GET
router.get('/test', userController.getTest);
router.get('/getallusers/:page', userController.getAllUsers);

//POST
router.post('/signup', userController.signUp);
router.post('/signin', userController.signIn);
router.post('/useredit', userController.userEdit);
router.post('/superadmingiveaccess', userController.superAdminGiveAccess);
router.post('/adminedituser', userController.adminEditUser);

module.exports = router;