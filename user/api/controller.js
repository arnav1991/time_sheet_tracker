var user = {};
var userModel = require('../db_functions/user_db.js').user_operations;
var Promise = require("bluebird");
var sha = require("sha256");
var jwt = require("jsonwebtoken");
var errorMessage = "some error occurred";
var config = require('config');

/////////////////////////GET////////////////////////////

user.getTest = function(req, res) {
	res.setHeader('Content-Type', 'application/json');
	res.send(JSON.stringify({"name": "arnav"}));
}

user.getAllUsers = function(req, res) {
    Promise.all([userModel.findAll(req.params.page), userModel.findCount()])
        .then(
            function(result) {

                if(result[0]==null) {
                    res.status(500).send({message: "Page does not exists"});
                    return;
                }
                var sendResult = {
                                    "data":result[0],
                                    "totalCount":result[1]
                                 };
                res.setHeader('Content-Type', 'application/json');
                res.send( sendResult);
        })
        .catch(function(error) {
                    res.setHeader('Content-Type', 'application/json');
                    res.status(500).send({message: "Some error occurred"});
        });
}

//////////////////////POST/////////////////////////////

user.signUp = function(req, res) {
	var email = req.body.email.toLowerCase();
	var passwordHash = sha(req.body.password);
	userModel.findOne(email, {})
		.then(function(result) {
			if(result != null) {
                res.setHeader('Content-Type', 'application/json');
                res.status(500).send({message: "email id already exists"});
            }
            else {
            	var userData = {
            		email: email,
			        full_name: req.body.full_name.toLowerCase(),
			        address: req.body.address.toLowerCase(),
			        date_of_birth: req.body.date_of_birth.toLowerCase(),
			        entry_time: Date.now(),
			        password_hash: passwordHash,
			        admin: false
            	}
            	userModel.create(userData)
                    .then(function(response) {
                        res.setHeader('Content-Type', 'application/json');
                        var api_resp = {
                        	mongo_resp: response,
                        	message: "success"
                        }
                        res.status(200).send(JSON.stringify(api_resp));
                    })
                    .catch(function(error) {
                        res.setHeader('Content-Type', 'application/json');
                        res.status(500).send(JSON.stringify({message: errorMessage}));
                    });
            }
		})
		.catch(function(error) {
			res.setHeader('Content-Type', 'application/json');
            res.status(500).send(JSON.stringify({message: errorMessage}));                 
		});
}

user.signIn = function(req, res) {
	var passwordHash = sha(req.body.password);
	var email = req.body.email.toLowerCase();
	userModel.findOne(email, {})
		.then(function(response) {
			if(response == null) {
				res.setHeader('Content-Type', 'application/json');
                res.status(500).send({message: "email id does not exists"});
			}
			else if(response.password_hash == passwordHash) {
        		var jwt_token = jwt.sign({data: response.email}, config.get('secret'), { expiresIn: config.get('token_time') });
        		var api_resp = {
					mongo_resp: response,
					message: "success",
					token: jwt_token
				}
				res.setHeader('Content-Type', 'application/json');
                res.status(200).send(JSON.stringify(api_resp));
			}
			else {
				var api_resp = {
					message: "wrong password"
				}
				res.setHeader('Content-Type', 'application/json');
                res.status(500).send(JSON.stringify(api_resp));
			}
		})
		.catch(function(error) {
			res.setHeader('Content-Type', 'application/json');
            res.status(500).send(JSON.stringify({message: errorMessage}));
		}); 
}

user.userEdit = function(req, res) {
	var token = req.body.token || req.query.token || req.headers['x-access-token'];
	if(token) {
		 jwt.verify(token, config.get('secret'), function(err, decoded) {       
		 	if (err) {
        		return res.json({ message: 'Failed to authenticate token.' });       
        	} 
        	else if(!req.body.update.hasOwnProperty("admin") && !req.body.update.hasOwnProperty("role")) {
        		console.log(req.body.update.hasOwnProperty("admin"));
        		var email = req.body.email.toLowerCase();
        		var update = req.body.update;
        		userModel.findOneAndUpdate(email, update)
        			.then(function(response) {
        				if(response == null) {
        					res.setHeader('Content-Type', 'application/json');
            				res.status(500).send(JSON.stringify({message: errorMessage}));
        				}
        				else {
        					var api_resp = {
        						message: "success",
        						mongo_resp: response
        					}
        					res.setHeader('Content-Type', 'application/json');
                			res.status(200).send(JSON.stringify(api_resp));
        				}
        			})
        			.catch(function(error) {
						res.setHeader('Content-Type', 'application/json');
            			res.status(500).send(JSON.stringify({message: errorMessage}));
					});      
      		}
      		else {
      			res.setHeader('Content-Type', 'application/json');
            	res.status(500).send(JSON.stringify({message: errorMessage}));
      		}
		});
	}
    else {
        res.setHeader('Content-Type', 'application/json');
        res.status(500).send(JSON.stringify({message: "token missing"}));
    }
}

user.superAdminGiveAccess = function(req, res) {
    var password_hash = sha(req.body.password).toUpperCase();
    var email = req.body.email.toLowerCase();
    var user_email = req.body.user_email.toLowerCase();
    if(email == config.get('super_admin_id') && password_hash == config.get('super_admin_password_hash')) {
        userModel.findOneAndUpdate(user_email, req.body.update)
            .then(function(response) {
                if(response == null) {
                    res.setHeader('Content-Type', 'application/json');
                    res.status(500).send(JSON.stringify({message: errorMessage}));
                }
                else {
                    var api_resp = {
                        message: "success",
                        mongo_resp: response
                    }
                    res.setHeader('Content-Type', 'application/json');
                    res.status(200).send(JSON.stringify(api_resp));
                }
            })
            .catch(function(error) {
                res.setHeader('Content-Type', 'application/json');
                res.status(500).send(JSON.stringify({message: errorMessage}));
            });
    }
    else {
        res.setHeader('Content-Type', 'application/json');
        res.status(500).send(JSON.stringify({message: "wrong super admin password or email"}));
    }
}

user.adminEditUser = function(req, res) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    var admin_email = req.body.admin_email.toLowerCase();
    var user_email = req.body.user_email.toLowerCase();
    if(token) {
         jwt.verify(token, config.get('secret'), function(err, decoded) {       
            if (err) {
                return res.json({ message: 'Failed to authenticate token.' });       
            }
            else {
                console.log(1);
                userModel.findOne(admin_email, {})
                    .then(function(response) {
                        if(response == null) { 
                            res.setHeader('Content-Type', 'application/json');
                            res.status(500).send(JSON.stringify({message: "admin does not exists"}));
                        }          
                        else if(response.admin) { 
                            userModel.findOneAndUpdate(user_email, req.body.update)
                                .then(function(response) { console.log(4);
                                    if(response == null) { 
                                        res.setHeader('Content-Type', 'application/json');
                                        res.status(500).send(JSON.stringify({message: "user does not exists"}));
                                    }
                                    else { 
                                        var api_resp = {
                                            message: "success",
                                            mongo_resp: response
                                        }
                                        res.setHeader('Content-Type', 'application/json');
                                        res.status(200).send(JSON.stringify(api_resp));
                                    }
                                })
                                .catch(function(error) { 
                                    res.setHeader('Content-Type', 'application/json');
                                    res.status(500).send(JSON.stringify({message: errorMessage}));
                                });
                        }
                        else {
                            res.setHeader('Content-Type', 'application/json');
                            res.status(500).send(JSON.stringify({message: errorMessage}));
                        }      
                    })
                    .catch(function(error) {
                        res.setHeader('Content-Type', 'application/json');
                        res.status(500).send(JSON.stringify({message: errorMessage}));
                    });
            }
        });
    }
    else {
        res.setHeader('Content-Type', 'application/json');
        res.status(500).send(JSON.stringify({message: "token missing"}));
    }
}



module.exports = user;