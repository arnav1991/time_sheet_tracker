var mongoDB = require('mongoose');
var Promise = require('bluebird');
var userSchema = require('../db_schema/user_schema.js');
var user = mongoDB.model("user",userSchema.userSchema);
user = Promise.promisifyAll(user);
var user_operations = {};

user_operations.findOne = function(emailId, toRetrieve) {
    return user.findOne({email: emailId}, toRetrieve);
}

user_operations.create = function(data) {
	return user.create(data);
}

user_operations.findOneAndUpdate = function(emailId, data) {
    return user.findOneAndUpdate({email: emailId}, {$set:data});
}

user_operations.findAll = function(page) {
    var skip = 10*(page-1);
    var limit = 10+(skip);
	 return user.find().skip(skip).limit(limit).sort({entry_time: -1});
}

user_operations.findCount = function() {
    return user.count();
}

module.exports.user_operations = user_operations;