var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
        email: String,
        full_name: String,
        address: String,
        date_of_birth: String,
        department: String,
        role: String,
        entry_time: Date,
        password_hash: String,
        admin: Boolean,
        project_information: String,
        role: String,
        project_name: String
});

module.exports.userSchema = userSchema;